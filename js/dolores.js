/*
 * Dolores App
 * Copyright (C) 2023 yPhil
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}

function updateServiceWorker() {
    if ('serviceWorker' in navigator) {
        console.log('Service worker is supported by the browser.');
        navigator.serviceWorker.ready.then(registration => {
            console.log('Service worker is ready.');
            registration.update().then(() => {
                console.log('Service worker update initiated.');
                if (navigator.serviceWorker.controller) {
                    console.log('Service worker controller is available.');
                    navigator.serviceWorker.controller.postMessage({action: 'skipWaiting'});
                } else {
                    console.log('Service worker controller is not available.');
                }
            }).catch(error => {
                console.log('Error occurred while updating service worker: ', error);
            });
        }).catch(error => {
            console.log('Service worker is not ready: ', error);
        });
    } else {
        console.log('Service worker is not supported by the browser.');
    }
}

document.getElementById('update-button').addEventListener('click', updateServiceWorker);

const doloresVersion = "1.4.5";

// Controls
const workoutDecrement = document.getElementById('workout-decrement');
const workoutIncrement = document.getElementById('workout-increment');
const restDecrement = document.getElementById('rest-decrement');
const restIncrement = document.getElementById('rest-increment');
const cyclesDecrement = document.getElementById('cycles-decrement');
const cyclesIncrement = document.getElementById('cycles-increment');
const startPauseButton = document.getElementById('start-pause');
const startPauseButtonIcon = document.getElementById('icon-play-pause');
const startPauseButtons = document.querySelectorAll('.start-pause');
const abortWorkoutButton = document.getElementById('abort-workout');
const beep = document.getElementById('beep');
const beepRest = document.getElementById('beep-rest');
const beepHalf = document.getElementById('beep-half');
const beepEnd = document.getElementById('beep-end'); // Get the beep-end sound
const workoutInterval = document.getElementById('workout');
const restInterval = document.getElementById('rest');
const cycles = document.getElementById('cycles');

const versionNumberSpan = document.getElementById('version-number');

versionNumberSpan.textContent = doloresVersion;
document.title = 'Dolores ' + doloresVersion;

// Get the setup and working blocks
const setupBlock = document.getElementById('setup-block');
const workingBlock = document.getElementById('working-block');

// Get the working block elements
const workingValue = document.querySelector('.working-value');
const workingSubtitle = document.querySelector('#working-block .subtitle');
const cycleNumber = document.querySelector('.cycle-number');

// FUCKING Firefox
if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
  var style = document.createElement('style');
  style.innerHTML = 'table {height: 680px !important; min-height: 680px !important;}';
  document.head.appendChild(style);
}

function convertSecondsToMinutes(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;
  return minutes + ":" + (remainingSeconds < 10 ? "0" : "") + remainingSeconds;
}

function loadValue(key, callback) {
    let transaction = window.db.transaction(["values"], "readonly");
    let objectStore = transaction.objectStore("values");
    let request = objectStore.get(key);

    request.onsuccess = function() {
        callback(request.result);
    };

    request.onerror = function() {
        console.error("Error", request.error);
    };
}

function saveValue(key, value) {
    let transaction = window.db.transaction(["values"], "readwrite");
    let objectStore = transaction.objectStore("values");
    let request = objectStore.put(value, key);

    request.onsuccess = function() {
        console.log("Value saved");
    };

    request.onerror = function() {
        console.log("Error", request.error);
    };
}

if (window.db) {
    console.log('dbReady already');
    initialize();
} else {
    document.addEventListener('dbReady', function() {
        initialize();
    });
}

function initialize() {
    if (!window.db) {
        return;
    }

    loadValue('workoutInterval', function(value) {
        if (value !== undefined) {
            workoutInterval.textContent = convertSecondsToMinutes(value);
            workoutInterval.setAttribute('data-seconds', value);
        } else {
            workoutInterval.textContent = convertSecondsToMinutes(30);
            workoutInterval.setAttribute('data-seconds', 30);
            saveValue('workoutInterval', 30);
        }
    });

    loadValue('restInterval', function(value) {
        if (value !== undefined) {
            restInterval.textContent = convertSecondsToMinutes(value);
            restInterval.setAttribute('data-seconds', value);
        } else {
            restInterval.textContent = convertSecondsToMinutes(20);
            restInterval.setAttribute('data-seconds', 20);
            saveValue('restInterval', 30);
        }
    });

    loadValue('cycles', function(value) {
        if (value !== undefined) {
            cycles.textContent = value;
        } else {
            cycles.textContent = 6;
        }
    });
}

// Create variables to store the interval ID, the current state, and the initial values
let intervalId = null;
let isWorkout = true;
let currentCycle = 0;
let shouldReset = false;
let initialWorkoutInterval;
let initialRestInterval;
let initialCycles;

// Add event listeners to the buttons
workoutDecrement.addEventListener('click', decrementWorkout);
workoutIncrement.addEventListener('click', incrementWorkout);
restDecrement.addEventListener('click', decrementRest);
restIncrement.addEventListener('click', incrementRest);
cyclesDecrement.addEventListener('click', decrementCycles);
cyclesIncrement.addEventListener('click', incrementCycles);

// startPauseButton.addEventListener('click', startPauseWorkout);

// Add an event listener to the start/pause buttons
startPauseButtons.forEach(button => {
    button.addEventListener('click', startPauseWorkout);
});

// Add an event listener to the abort workout button
abortWorkoutButton.addEventListener('click', abortWorkout);

let savePromise = Promise.resolve();

function saveWorkoutTime() {
    savePromise = savePromise.then(() => new Promise((resolve, reject) => {
        // Start a new transaction
        let transaction = window.db.transaction(["workouts"], "readwrite");

        // Get the workouts object store
        let objectStore = transaction.objectStore("workouts");

        // Get the current date and normalize to the start of the day
        let currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        let date = currentDate.getFullYear() + '-' + String(currentDate.getMonth() + 1).padStart(2, '0') + '-' + String(currentDate.getDate()).padStart(2, '0');

        // Get the workout for the current date
        let getRequest = objectStore.get(date);

        getRequest.onsuccess = function() {
            let data = getRequest.result;
            if (data) {
                // If a workout exists for the current date, increment the workout time
                data.workoutTime += 1;
                let putRequest = objectStore.put(data);
                putRequest.onerror = function() {
                    console.log("Error", putRequest.error);
                    reject();
                };
                putRequest.onsuccess = function() {
                    resolve();
                };
            } else {
                // If no workout exists for the current date, add a new workout
                let addRequest = objectStore.add({date: date, workoutTime: 1});
                addRequest.onerror = function() {
                    console.log("Error", addRequest.error);
                    reject();
                };
                addRequest.onsuccess = function() {
                    resolve();
                };
            }
        };

        getRequest.onerror = function() {
            console.log("Error", getRequest.error);
            reject();
        };
    }));
}

let isInitialPress = true;

function startPauseWorkout() {
    const totalCycles = document.querySelector('.total-cycles');

    // If the workout interval is 0, do nothing and return immediately
    if (parseInt(workoutInterval.getAttribute('data-seconds')) === 0) {
        return;
    }

    // If it's the first press, save the current values as the initial values
    if (isInitialPress) {
        initialWorkoutInterval = parseInt(workoutInterval.getAttribute('data-seconds'));
        initialRestInterval = parseInt(restInterval.getAttribute('data-seconds'));
        initialCycles = parseInt(cycles.textContent);
        isInitialPress = false;
        totalCycles.textContent = initialCycles;
    }

    if (intervalId !== null) {
        // Pause the workout
        clearInterval(intervalId);
        intervalId = null;

        startPauseButtons.forEach(button => {
            button.children[0].className = 'icon-play';
        });

        abortWorkoutButton.style.display = 'block';
    } else {
        // workoutContainer.classList.add('has-background-info');

        // Resume the workout
        intervalId = setInterval(() => {

            // Get the current interval element and beep sound
            const currentInterval = isWorkout ? workoutInterval : restInterval;
            const currentBeep = isWorkout ? beep : beepRest;

            // Update the working block elements
            workingValue.textContent = currentInterval.textContent;
            workingSubtitle.textContent = isWorkout ? 'Workout' : 'Rest';

            // Check if it's the last rest cycle
            const isLastRest = currentCycle + 1 == initialCycles && !isWorkout;

            // Check if the current interval is less than or equal to 5
            const isIntervalEnding = parseInt(currentInterval.getAttribute('data-seconds')) <= 5;

            // Play the beep sound or log 'last rest' based on the conditions
            if (isLastRest) {

                // Show the setup block and hide the working block
                setupBlock.style.display = 'block';
                workingBlock.style.display = 'none';

                return true;
            } else if (isIntervalEnding) {
                console.log('currentInterval: ', currentInterval.getAttribute('data-seconds'));
                beepEnd.play();
            } else {
                currentBeep.play();
            }

            // Decrement the current interval
            let value = parseInt(currentInterval.getAttribute('data-seconds'));
            if (value > 1) {

                if (isLastRest) {
                    return true;
                } else {
                    currentInterval.setAttribute('data-seconds', value - 1);
                    currentInterval.textContent = convertSecondsToMinutes(value - 1);
                }


            } else {
                // If the current interval has reached 1, switch to the other interval
                isWorkout = !isWorkout;

                // If we've just finished a rest interval, increment the cycle count and decrement the cycles
                if (isWorkout) {
                    currentCycle++;
                    if (currentCycle === initialCycles - 1) {
                        console.log(`Start of last workout cycle ${currentCycle + 1}`); // Log the start of the last workout cycle
                    }
                    cycles.textContent = parseInt(cycles.textContent) - 1;
                }

                // If we've finished all cycles, stop the timer
                if (currentCycle >= initialCycles) {
                    clearInterval(intervalId);
                    intervalId = null;
                    startPauseButtonIcon.className = 'icon-play';
                    isWorkout = true;
                    currentCycle = 0;
                    workoutInterval.textContent = convertSecondsToMinutes(initialWorkoutInterval);
                    workoutInterval.setAttribute('data-seconds', initialWorkoutInterval);
                    restInterval.textContent = convertSecondsToMinutes(initialRestInterval);
                    restInterval.setAttribute('data-seconds', initialRestInterval);
                    cycles.textContent = initialCycles;
                    abortWorkoutButton.style.display = 'none'; // Hide the abort-workout button

                    // Show the setup block and hide the working block
                    setupBlock.style.display = 'block';
                    workingBlock.style.display = 'none';


                } else {
                    // If shouldReset is true, reset the interval to its initial value
                    currentInterval.textContent = isWorkout ? initialRestInterval : initialWorkoutInterval;
                    currentInterval.setAttribute('data-seconds', isWorkout ? initialRestInterval : initialWorkoutInterval);
                }
            }
            // Save workout time to the database every second during the workout
            if (isWorkout) {
                saveWorkoutTime();
            }
        }, 1000); // Run every second

        startPauseButtons.forEach(button => {
            button.children[0].className = 'icon-pause';
        });

        // Hide the setup block and show the working block
        setupBlock.style.display = 'none';
        workingBlock.style.display = 'block';
        abortWorkoutButton.style.display = 'none';
    }
}

function abortWorkout() {
    // Reset the workout, rest, and cycle intervals to their initial values
    startPauseButtonIcon.className = 'icon-play';
    isWorkout = true;
    currentCycle = 0;
    workoutInterval.textContent = convertSecondsToMinutes(initialWorkoutInterval);
    workoutInterval.setAttribute('data-seconds', initialWorkoutInterval);
    restInterval.textContent = convertSecondsToMinutes(initialRestInterval);
    restInterval.setAttribute('data-seconds', initialRestInterval);
    cycles.textContent = initialCycles.toString();

    // Hide the abort-workout button
    abortWorkoutButton.style.display = 'none';

    setupBlock.style.display = 'block';
    workingBlock.style.display = 'none';
    // Reset the values
    workingSubtitle.textContent = 'Workout';
    workingValue.textContent = '0.0';
    cycleNumber.textContent = '1';
}

function decrementWorkoutModal() {
    let value = parseInt(modalWorkoutValue.getAttribute('data-seconds').toString());
    if (value > 0) {
        modalWorkoutValue.textContent = convertSecondsToMinutes(value - 5);
        modalWorkoutValue.setAttribute('data-seconds', value - 5);
    } else {
        modalWorkoutValue.textContent = 0;
        modalWorkoutValue.setAttribute('data-seconds', 0);
    }

    modalWorkoutValue.setAttribute('data-seconds', modalWorkoutValue.getAttribute('data-seconds'));
    // saveValue('modalWorkoutValue', modalWorkoutValue.getAttribute('data-seconds'));
}

function incrementWorkoutModal () {
    let value = parseInt(modalWorkoutValue.getAttribute('data-seconds'));
    modalWorkoutValue.textContent = convertSecondsToMinutes(value + 5);
    modalWorkoutValue.setAttribute('data-seconds', value + 5);
    // saveValue('modalWorkoutValue', modalWorkoutValue.getAttribute('data-seconds'));
}

function decrementWorkout() {
    let value = parseInt(workoutInterval.getAttribute('data-seconds').toString());
    if (value > 0) {
        workoutInterval.textContent = convertSecondsToMinutes(value - 5);
        workoutInterval.setAttribute('data-seconds', value - 5);
    } else {
        workoutInterval.textContent = '0:00';
        workoutInterval.setAttribute('data-seconds', 0);
    }

    workoutInterval.setAttribute('data-seconds', workoutInterval.getAttribute('data-seconds'));
    saveValue('workoutInterval', workoutInterval.getAttribute('data-seconds'));
}

function incrementWorkout() {
    let value = parseInt(workoutInterval.getAttribute('data-seconds'));
    workoutInterval.textContent = convertSecondsToMinutes(value + 5);
    workoutInterval.setAttribute('data-seconds', value + 5);
    saveValue('workoutInterval', workoutInterval.getAttribute('data-seconds'));
}

function decrementRest() {
    let value = parseInt(restInterval.getAttribute('data-seconds'));
    if (value > 0) {
        restInterval.textContent = convertSecondsToMinutes(value - 5);
        restInterval.setAttribute('data-seconds', value - 5);
    } else {
        restInterval.textContent = '0:00';
        restInterval.setAttribute('data-seconds', 0);
    }

    restInterval.setAttribute('data-seconds', restInterval.getAttribute('data-seconds'));
    saveValue('restInterval', restInterval.getAttribute('data-seconds'));
}

function incrementRest() {
    let value = parseInt(restInterval.getAttribute('data-seconds'));
    restInterval.textContent = convertSecondsToMinutes(value + 5);
    restInterval.setAttribute('data-seconds', value + 5);
    saveValue('restInterval', restInterval.getAttribute('data-seconds'));
}

function decrementCycles() {
    let value = parseInt(cycles.textContent);
    if (value > 0) {
        cycles.textContent = value - 1;
    } else {
        cycles.textContent = 0;
    }

    saveValue('cycles', cycles.textContent);
}

function incrementCycles() {
    let value = parseInt(cycles.textContent);
    cycles.textContent = value + 1;
    saveValue('cycles', cycles.textContent);
}
