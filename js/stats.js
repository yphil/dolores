function addWorkout(workoutTime) {
    // Start a new transaction
    let transaction = window.db.transaction(["workouts"], "readwrite");

    // Get the workouts object store
    let objectStore = transaction.objectStore("workouts");

    // Add the workout to the object store
    let request = objectStore.add({
        date: new Date().toISOString().split('T')[0], // Get the current date in YYYY-MM-DD format
        workoutTime: workoutTime
    });

    request.onsuccess = function(event) {
        console.log("Workout added to the database.");
    };

    request.onerror = function(event) {
        console.log("Error adding workout to the database", event);
    };
}

function getWorkouts() {
    // Start a new transaction
    let transaction = window.db.transaction(["workouts"], "readonly");

    // Get the workouts object store
    let objectStore = transaction.objectStore("workouts");

    // Get all workouts from the object store
    let request = objectStore.getAll();

    request.onsuccess = function(event) {
        console.log("Workouts:", request.result);
    };

    request.onerror = function(event) {
        console.log("Error getting workouts from the database", event);
    };
}


function openTab(evt, tabName) {
    // Declare all variables
    let i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("content-container");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        tablinks[i].parentNode.classList.remove('is-active');
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        tablinks[i].parentNode.classList.remove('is-active');
    }
    // evt.currentTarget.className += " active";
    evt.currentTarget.parentNode.classList.add('is-active');
    displayDayStats();
}

let currentDate = new Date();

function displayPrevDayStats() {
    // Subtract one day from the current date
    currentDate.setDate(currentDate.getDate() - 1);
    displayDayStats();
}

function displayTodayStats() {
    // Set the current date to today
    currentDate = new Date();
    displayDayStats();
}

function displayNextDayStats() {
    // Add one day to the current date
    currentDate.setDate(currentDate.getDate() + 1);
    displayDayStats();
}

function secondsToMinutes(seconds) {
    return Math.floor(seconds / 60) + "m " + (seconds % 60) + "s";
}

function displayDayStats() {
    let transaction = window.db.transaction(["workouts"], "readonly");
    let objectStore = transaction.objectStore("workouts");
    let request = objectStore.getAll();

    request.onsuccess = function(event) {
        // Get the current date and normalize to the start of the day
        // let currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);

        // Get the current date in YYYY-MM-DD format in the local timezone
        let currentDateString = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1).toString().padStart(2, '0') + '-' + currentDate.getDate().toString().padStart(2, '0');

        // Filter out the workouts that were not done on the current date
        let currentDayWorkouts = request.result.filter(workout => workout.date === currentDateString);

        // Calculate the total workout time for the current date
        let totalWorkoutTime = currentDayWorkouts.reduce((total, workout) => total + workout.workoutTime, 0);
        // Round to 0 decimal places
        totalWorkoutTime = totalWorkoutTime.toFixed(0);

        let dayYearElement = document.getElementById('day-year');
        if (dayYearElement) {
            dayYearElement.textContent = currentDateString + ': ' + secondsToMinutes(totalWorkoutTime);
        }
    };

    request.onerror = function(event) {
        console.log("Error getting workouts from the database", event);
    };
}


function displayPrevWeekStats() {
    // Subtract seven days from the currentDate
    currentDate.setDate(currentDate.getDate() - 7);
    // Call displayWeekStats to refresh the display
    displayWeekStats();
}

function displayNextWeekStats() {
    // Add seven days to the currentDate
    currentDate.setDate(currentDate.getDate() + 7);
    // Call displayWeekStats to refresh the display
    displayWeekStats();
}

// This function is used to display the statistics for the current week
function displayWeekStats() {
    // Start a new transaction
    let transaction = window.db.transaction(["workouts"], "readonly");

    // Get the workouts object store
    let objectStore = transaction.objectStore("workouts");

    // Get all workouts from the object store
    let request = objectStore.getAll();

    request.onsuccess = function(event) {
        // Get the current date and normalize to the start of the day
        let today = new Date(currentDate); // Use the global currentDate variable

        // Set the hours, minutes, seconds, and milliseconds to 0
        today.setHours(0, 0, 0, 0);

        // Get the first day of the current week
        let firstDayOfWeek = new Date(today.setDate(today.getDate() - today.getDay()));

        // Get the first day of the next week
        let firstDayOfNextWeek = new Date(currentDate);
        firstDayOfNextWeek.setDate(firstDayOfNextWeek.getDate() - firstDayOfNextWeek.getDay() + 7);

        // Filter out the workouts that were not done this week
        let thisWeeksWorkouts = request.result.filter(workout => {
            let workoutDate = new Date(workout.date);
            // Check that the workout date is within the current week
            return workoutDate >= firstDayOfWeek && workoutDate < firstDayOfNextWeek;
        });

        // Calculate the total workout time for this week
        let totalWorkoutTime = thisWeeksWorkouts.reduce((total, workout) => total + workout.workoutTime, 0);

        // Display the total workout time for this week
        document.getElementById('week-stats').textContent = 'Total workout time this week: ' + totalWorkoutTime + ' seconds';
    };

    request.onerror = function(event) {
        console.log("Error getting workouts from the database", event);
    };
}

function displayMonthStats() {
    // Start a new transaction
    let transaction = window.db.transaction(["workouts"], "readonly");

    // Get the workouts object store
    let objectStore = transaction.objectStore("workouts");

    // Get all workouts from the object store
    let request = objectStore.getAll();

    request.onsuccess = function(event) {
        // Get the current date and normalize to the start of the day
        let today = new Date();
        today.setHours(0, 0, 0, 0);

        // Get the first day of the current month
        let firstDayOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);

        // Filter out the workouts that were not done this month
        let thisMonthsWorkouts = request.result.filter(workout => {
            let workoutDate = new Date(workout.date);
            return workoutDate >= firstDayOfMonth;
        });

        // Calculate the total workout time for this month
        let totalWorkoutTime = thisMonthsWorkouts.reduce((total, workout) => total + workout.workoutTime, 0);

        // Display the total workout time for this month
        document.getElementById('month-stats').textContent = 'Total workout time this month: ' + totalWorkoutTime + ' seconds';
    };

    request.onerror = function(event) {
        console.log("Error getting workouts from the database", event);
    };
}

function insertWorkoutData(date, workoutTime) {
    return new Promise((resolve, reject) => {
        let openRequest = indexedDB.open("WorkoutDB", 1);

        openRequest.onsuccess = function() {
            let db = openRequest.result;
            let transaction = window.db.transaction("workouts", "readwrite");
            let workouts = transaction.objectStore("workouts");
            let request = workouts.put({date: date, workoutTime: workoutTime});

            request.onsuccess = function() {
                resolve(request.result);
            };

            request.onerror = function() {
                reject(request.error);
            };
        };

        openRequest.onerror = function() {
            reject(openRequest.error);
        };
    });
}

function insertWorkoutData(date, workoutTime) {
  return new Promise((resolve, reject) => {
    let openRequest = indexedDB.open("WorkoutDB", 1);

    openRequest.onsuccess = function () {
      let db = openRequest.result;
      let transaction = db.transaction("workouts", "readwrite");
      let workouts = transaction.objectStore("workouts");

      let getRequest = workouts.get(date);
      getRequest.onsuccess = function () {
        let existingData = getRequest.result;
        if (existingData) {
          let updateRequest = workouts.put({ date: date, workoutTime: existingData.workoutTime + workoutTime });
          updateRequest.onsuccess = function () {
            resolve(updateRequest.result);
          };
          updateRequest.onerror = function () {
            reject(updateRequest.error);
          };
        } else {
          let addRequest = workouts.add({ date: date, workoutTime: workoutTime });
          addRequest.onsuccess = function () {
            resolve(addRequest.result);
          };
          addRequest.onerror = function () {
            reject(addRequest.error);
          };
        }
      };

      getRequest.onerror = function () {
        reject(getRequest.error);
      };
    };

    openRequest.onerror = function () {
      reject(openRequest.error);
    };
  });
}


function eraseDatabase() {
    // Check if window.db is defined
    if (!window.db) {
        let openRequest = indexedDB.open("WorkoutDB", 1);

        openRequest.onsuccess = function() {
            window.db = openRequest.result;
            clearDatabase();
        };

        openRequest.onerror = function(event) {
            console.log("Unable to open database.", event);
        };
    } else {
        clearDatabase();
    }

    function clearDatabase() {
        // Open a transaction on your workouts object store
        let transaction = window.db.transaction(["workouts"], "readwrite");
        let objectStore = transaction.objectStore("workouts");

        // Make a request to clear all the data
        let request = objectStore.clear();

        request.onsuccess = function(event) {
            console.log("Successfully cleared all data from the database.");
        };

        request.onerror = function(event) {
            console.log("Unable to clear data from the database.");
        };
    }
}

function openModal(date, seconds) {

    const modal = document.querySelector('.modal');

    const modalWorkoutDecrement = modal.querySelector('#modal-workout-decrement');
    const modalWorkoutIncrement = modal.querySelector('#modal-workout-increment');
    const modalWorkoutValue = modal.querySelector('#modal-workout-value');

    modalWorkoutValue.setAttribute('data-seconds', seconds);

    function decrementWorkoutModal() {
        let value = parseInt(modalWorkoutValue.getAttribute('data-seconds').toString());
        if (value > 0) {
            modalWorkoutValue.textContent = convertSecondsToMinutes(value - 5);
            modalWorkoutValue.setAttribute('data-seconds', value - 5);
        } else {
            modalWorkoutValue.textContent = '0:00';
            modalWorkoutValue.setAttribute('data-seconds', 0);
        }

        modalWorkoutValue.setAttribute('data-seconds', modalWorkoutValue.getAttribute('data-seconds'));
    }

    function incrementWorkoutModal () {
        let value = parseInt(modalWorkoutValue.getAttribute('data-seconds'));
        modalWorkoutValue.textContent = convertSecondsToMinutes(value + 5);
        modalWorkoutValue.setAttribute('data-seconds', value + 5);
    }

    const modalContent = modal.querySelector('.modal-content');
    const modalCardTitle = modal.querySelector('.modal-card-title');
    const modalWorkoutValueButton = modal.querySelector('.modal-workout-values');

    const modalDeleteButton = modal.querySelector('.delete');
    const modalCancelButton = modal.querySelector('.cancel');


    modalDeleteButton.addEventListener('click', closeModal);
    modalCancelButton.addEventListener('click', closeModal);

    modalWorkoutDecrement.addEventListener('click', decrementWorkoutModal);
    modalWorkoutIncrement.addEventListener('click', incrementWorkoutModal);

    modalCardTitle.textContent = date;
    modalWorkoutValueButton.textContent = convertSecondsToMinutes(seconds);

    modal.classList.add('is-active');
}

// Function to close the modal
function closeModal() {
    // Get the modal element
    var modal = document.querySelector('.modal');

    // Remove the 'is-active' class to close the modal
    modal.classList.remove('is-active');
}

// Add event listener to the td element when the DOM is ready
document.addEventListener('DOMContentLoaded', function() {
    // Get the td element
    let statsTableWorkoutDayTd = document.querySelector('.workout-day');
    let statsTableTd = document.querySelector('.stats-table-td');

    // // Attach the event listener to the td element
    // if (statsTableWorkoutDayTd) {
    //     statsTableWorkoutDayTd.addEventListener('click', openModal);
    //     console.log('yoooo: ');
    // }

    if (statsTableTd && statsTableWorkoutDayTd) {
        const computedStyle = getComputedStyle(statsTableWorkoutDayTd);
        const backgroundColor = computedStyle.backgroundColor;
        statsTableTd.style.border = `1px solid ${backgroundColor}`;
        statsTableWorkoutDayTd.style.border = `1px solid ${backgroundColor}`;
    }

});

let closeElement = document.getElementById('close');
if (closeElement) {
    closeElement.addEventListener('click', function() {
        document.getElementById('modal').style.display = 'none';
    });
}

let prevDayElement = document.getElementById('prev-day');
if (prevDayElement) {
    prevDayElement.addEventListener('click', displayPrevDayStats);
}

let todayDayElement = document.getElementById('today-day');
if (todayDayElement) {
    todayDayElement.addEventListener('click', displayTodayStats);
}

let nextDayElement = document.getElementById('next-day');
if (nextDayElement) {
    nextDayElement.addEventListener('click', displayNextDayStats);
}

document.addEventListener('dbReady', displayTodayStats);
