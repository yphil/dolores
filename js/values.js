let db;

let openRequest = indexedDB.open("WorkoutDB", 1);

openRequest.onupgradeneeded = function() {
  db = openRequest.result;
  if (!db.objectStoreNames.contains('values')) {
    db.createObjectStore('values');
  }
};

openRequest.onsuccess = function() {
  db = openRequest.result;
  // initializeDolores();
  document.dispatchEvent(new Event('dbReady'));
};

openRequest.onerror = function() {
  console.log("Error", openRequest.error);
};

// document.getElementById('workout-increment').onclick = function() {
//   let workout = document.getElementById('workout');
//   workout.textContent = parseInt(workout.textContent) + 1;
//   saveValue('workout', workout.textContent);
// };

document.getElementById('modal-workout-increment').onclick = function() {
  let workout = document.getElementById('workout');
  workout.textContent = parseInt(workout.textContent) + 1;
  saveValue('workout', workout.textContent);
};

function loadValue(key, callback) {
  let transaction = db.transaction(["values"], "readonly");
  let objectStore = transaction.objectStore("values");
  let request = objectStore.get(key);

  request.onsuccess = function() {
    callback(request.result);
  };

  request.onerror = function() {
    console.log("Error", request.error);
  };
}

function saveValue(key, value) {
  let transaction = db.transaction(["values"], "readwrite");
  let objectStore = transaction.objectStore("values");
  let request = objectStore.put(value, key);

  request.onsuccess = function() {
    console.log("Value saved");
  };

  request.onerror = function() {
    console.log("Error", request.error);
  };
}
