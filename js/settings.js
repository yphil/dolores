var lightTheme = 'css/dolores-bulma-light.css';
var darkTheme = 'css/dolores-bulma-dark.css';

function switchTheme(theme) {
    // Get the existing theme link element
    var themeLink = document.getElementById('theme-link');

    // Create a new link element for the new theme
    var newThemeLink = document.createElement('link');
    newThemeLink.id = 'theme-link';
    newThemeLink.rel = 'stylesheet';
    newThemeLink.href = theme;

    // Replace the existing theme link with the new one
    themeLink.parentNode.replaceChild(newThemeLink, themeLink);
}

function saveSettings() {

    // Get the selected theme
    let themeRadioButtons = document.getElementsByName('theme');
    let selectedTheme = '';

    for (let i = 0; i < themeRadioButtons.length; i++) {
        if (themeRadioButtons[i].checked) {
            selectedTheme = themeRadioButtons[i].value;
            break;
        }
    }

    // Check if the theme has changed
    let currentTheme = getCurrentTheme();

    if (selectedTheme !== currentTheme) {
        if (selectedTheme === 'light') {
            switchTheme(lightTheme);
        } else if (selectedTheme === 'dark') {
            switchTheme(darkTheme);
        }
    }

    // Update and persist the theme settings
    let themeSettings = {
        theme: selectedTheme
    };

    let transaction = window.db.transaction(["settings"], "readwrite");
    let store = transaction.objectStore("settings");
    let request = store.put(themeSettings, 'themeSettings');

    request.onsuccess = function(event) {
        console.log('Theme settings saved successfully');
    };

    request.onerror = function(event) {
        console.log('Error saving theme settings', event);
    };
}

function getCurrentTheme() {
    // Get the current theme from the theme link element
    var themeLink = document.getElementById('theme-link');
    var theme = themeLink.getAttribute('href');

    if (theme === lightTheme) {
        return 'light';
    } else if (theme === darkTheme) {
        return 'dark';
    }

    return '';
}

function loadSettings() {
    let transaction = window.db.transaction(["settings"], "readonly");
    let store = transaction.objectStore("settings");

    // Load the sound settings
    let soundSettingsRequest = store.get('soundSettings');

    soundSettingsRequest.onsuccess = function(event) {
        let soundSettings = soundSettingsRequest.result;
        let defaultChecked = true;

        if (!soundSettings || Object.keys(soundSettings).length === 0) {
            // Set all checkboxes to checked if soundSettings is empty
            document.getElementById('playBeep').checked = defaultChecked;
            document.getElementById('playBeepRest').checked = defaultChecked;
            document.getElementById('playBeepEnd').checked = defaultChecked;
        } else {
            document.getElementById('playBeep').checked = soundSettings.playBeep;
            document.getElementById('playBeepRest').checked = soundSettings.playBeepRest;
            document.getElementById('playBeepEnd').checked = soundSettings.playBeepEnd;
        }
    };

    soundSettingsRequest.onerror = function(event) {
        console.log('Error loading sound settings', event);
    };

    // Load the theme settings
    let themeSettingsRequest = store.get('themeSettings');

    themeSettingsRequest.onsuccess = function(event) {
        let themeSettings = themeSettingsRequest.result;
        let defaultTheme = 'light';

        if (!themeSettings || Object.keys(themeSettings).length === 0) {
            // Set the default theme to light if themeSettings is empty
            document.querySelector('input[name="theme"][value="' + defaultTheme + '"]').checked = true;
            switchTheme(lightTheme);
        } else {
            // Select the theme based on the stored setting
            let selectedTheme = themeSettings.theme;
            document.querySelector('input[name="theme"][value="' + selectedTheme + '"]').checked = true;

            if (selectedTheme === 'light') {
                switchTheme(lightTheme);
            } else if (selectedTheme === 'dark') {
                switchTheme(darkTheme);
            }
        }
    };

    themeSettingsRequest.onerror = function(event) {
        console.log('Error loading theme settings', event);
    };
}


function exportDatabase() {
    let transaction = window.db.transaction(["workouts", "values", "settings"], "readonly");
    let workoutsStore = transaction.objectStore("workouts");
    let valuesStore = transaction.objectStore("values");
    let settingsStore = transaction.objectStore("settings");

    // Get all data from the stores
    Promise.all([
        getDataFromStore(workoutsStore),
        getDataFromStore(valuesStore),
        getDataFromStore(settingsStore)
    ]).then(([workouts, values, settings]) => {
        let exportedData = {
            workouts: workouts,
            values: values,
            settings: settings
        };

        // Convert the data to a JSON string
        let exportedDataStr = JSON.stringify(exportedData);

        // Download the data as a JSON file
        let a = document.createElement('a');
        a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(exportedDataStr));
        a.setAttribute('download', 'dolores-satatistics.json');
        a.click();
    });

    function getDataFromStore(store) {
        return new Promise((resolve, reject) => {
            let request = store.getAll();
            request.onsuccess = function(event) {
                resolve(request.result);
            };
            request.onerror = function(event) {
                reject(request.error);
            };
        });
    }
}

function importDatabase(event) {
    let file = event.target.files[0];
    if (!file) {
        console.log('No file selected');
        return;
    }

    let reader = new FileReader();
    reader.onload = function(event) {
        let importedData = JSON.parse(event.target.result);

        let transaction = window.db.transaction(["workouts", "values", "settings"], "readwrite");
        let workoutsStore = transaction.objectStore("workouts");
        let valuesStore = transaction.objectStore("values");
        let settingsStore = transaction.objectStore("settings");

        // Clear the stores
        workoutsStore.clear();
        valuesStore.clear();
        settingsStore.clear();

        // Add the imported data to the stores
        importedData.workouts.forEach(workout => workoutsStore.add(workout));
        importedData.values.forEach(value => valuesStore.add(value));
        importedData.settings.forEach(setting => settingsStore.add(setting));

        console.log('Database imported successfully');
    };
    reader.readAsText(file);
}

document.getElementById('importButton').addEventListener('click', function() {
    document.getElementById('importFile').click();
});

document.getElementById('exportButton').addEventListener('click', exportDatabase);
document.getElementById('importFile').addEventListener('change', importDatabase);

document.addEventListener('dbReady', loadSettings);

// var settingsPageLoaded = document.querySelector('#settings-div');
// if (settingsPageLoaded) {
//    loadSettings();
// }
